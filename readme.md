Rename `.env-sample` to `.env` and set variables

Install dependencies
```
npm i
```

Launch
```
node index.js
```
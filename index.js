import open from 'open';
import express from 'express';
import * as dotenv from 'dotenv';
import {LATEST_API_VERSION} from '@shopify/shopify-api';
import {shopifyApp} from '@shopify/shopify-app-express';
import {SQLiteSessionStorage} from '@shopify/shopify-app-session-storage-sqlite';
import {restResources} from '@shopify/shopify-api/rest/admin/2023-01';
import GDPRWebhookHandlers from './gdpr.js';

dotenv.config();

const app = express();
const shopify = shopifyApp({
	api: {
		apiKey: process.env.SHOPIFY_API_KEY,
		apiSecretKey: process.env.SHOPIFY_API_SECRET_KEY,
		scopes: ['read_products'],
		hostName: process.env.APP_HOST,
		apiVersion: LATEST_API_VERSION,
		isEmbeddedApp: false,
		restResources
	},
	auth: {
		path: '/api/auth',
		callbackPath: '/api/auth/callback'
	},
	webhooks: {
		path: '/api/webhooks'
	},
	sessionStorage: new SQLiteSessionStorage(process.env.DB_PATH)
});

// Set up Shopify authentication
app.get(shopify.config.auth.path, shopify.auth.begin());

app.get(
	shopify.config.auth.callbackPath,
	shopify.auth.callback(),
	shopify.redirectToShopifyOrAppRoot()
);

// Set up webhook handling
app.post(
	shopify.config.webhooks.path,
	shopify.processWebhooks({webhookHandlers: GDPRWebhookHandlers})
);

// Middleware to check session is authenticated
app.use('/api/*', shopify.validateAuthenticatedSession());

app.use(express.json());

const redirectToAuth = res => {
	res.redirect(`${shopify.config.auth.path}?shop=${process.env.SHOPIFY_SHOP}`);
};

app.get('/', async (req, res) => {
	const {api, config} = shopify;
	const sessions = await config.sessionStorage.findSessionsByShop(process.env.SHOPIFY_SHOP);

	if (sessions.length === 0) {
		return redirectToAuth(res);
	}

	const [session] = sessions;

	// Session may exist, but the application can be deleted in Shopify shop
	// so handle REST request in try-catch block, and if session is invalid redirect to auth

	try {
		const products = await api.rest.Product.all({
			session: session
		});

		return res.status(200).send({
			status: 'Authenticated',
			session: session,
			products
		});
	} catch (e) {
		await config.sessionStorage.deleteSession(session.id);
		return redirectToAuth(res);
	}
});

app.listen(process.env.PORT, async () => {
	await open(`https://${process.env.APP_HOST}`);
});
